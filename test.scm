( define-module
	( test)
	#:use-module ( gnu packages)
	#:use-module ( gnu packages shellutils)
	#:use-module ( gnu packages base)
	#:use-module ( guix packages)
	#:use-module ( guix build-system copy)
	#:use-module ( guix git-download)
	#:use-module ( guix gexp)
	#:use-module (
		( guix licenses)
		#:prefix license:
	)
)

( define-public
	test
	( let
		(
			( commit "16fec2068ec03bf2420188edd9840eaa241fbcf7")
		)
		( package
			( name "test")
			( version
				"0.0"
			)
			( source
				( origin
					( method git-fetch)
					( uri
						( git-reference
							( commit commit)
							( url "https://github.com/abcdw/rde")
							( recursive? #t) ; naughty line
						)
					)
					; ( patch-inputs
					; 	( list
					; 		shell-functools
					; 		sed
					; 		coreutils
					; 	)
					; )
					( sha256
						( base32 "0x208rdyvxgww84j6cf4f02zy80ha22x066pfpl91zv8366id0lc")
					)
				)
			)
			; ( inputs
			; 	( list
			; 		shell-functools
			; 		sed
			; 		coreutils
			; 	)
			; )
			( build-system copy-build-system)
			(arguments
				(list
					#:install-plan `( list
						( list
							"doc"
							"/newDoccy"
						)
					)
				)
			)
			( home-page "")
			( synopsis "")
			( description "")
			( license license:mpl2.0)
		)
	)
)
